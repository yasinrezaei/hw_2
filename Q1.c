
/*

سلام من تمامی حالت های وارد کردن یک عدد را درنظر گرفتم به همین خاطر ممکن است که کد کمی پیچیده باشد
واینکه به صورت برنامه کامل هست و ورودی هم میگیره
*/
#include<stdio.h>
void Generate_smallest_number(int arr[],int n);

int main(){
    
    int arr[10];
    int i=0;
    char c;
    printf("Enter your num:");
    while ((c=getchar())!='\n')
    {
        arr[i]=c-'0';
        i+=1; 
    }
  
    Generate_smallest_number(arr,i);

    return 0;
}
Generate_smallest_number(int arr[],int n){
    
    int j,r;
 
    for(j=0;j<n;j++){
        for(r=0;r<n-1;r++){
            if(arr[r]>arr[r+1]){
                int temp=arr[r];
                arr[r]=arr[r+1];
                arr[r+1]=temp;
            }
        }
        
    }
    int b;
    for(j=0;j<n;j++){
        if(arr[j]!=0){
            b=j;
            break;
        }
    }
    printf("Smallest num is: ");
    if(j==n){
        printf("0");
    }
    else{
        for(j=b;j<n;j++){
        printf("%d",arr[j]);
        }
    }    
}