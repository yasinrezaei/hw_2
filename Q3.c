#include<stdio.h>

int read_line(char line ,int max_length);
void printline(char line[],int len);

int main(){

    char line[1000];
    char new_line[1000];

    int len=readline(line,1000);
    int q=0;

    for(int i=0;i<len;i++){
        if(line[i]!=' ' && line[i]!='\t'){
            new_line[q]=line[i];
            q+=1;
        }
    }
    printline(new_line,q);
    return 0;

}

int readline(char line[],int max_length){
    int line_length=0;
    char c;

    while ((c=getchar())!='\n' && line_length<max_length)
    {
        line[line_length]=c;
        line_length+=1;
    }

    line[line_length]='\0';
    
    return line_length;
    
}

void printline(char line[],int len){
    for(int i=0;i<len;i++){
        printf("%c",line[i]);
}




}