#include<stdio.h>
#include<string.h>

int readarray(char arr[],int len);
void sort(char arr[],int len);
void printarray(char array[],int len);

int main(){

    char arr[1000];

    int len=readarray(arr,1000);
    sort(arr,len);
    printarray(arr,len);


    return 0;
}

void printarray(char array[],int len){
    for(int i=0;i<len;i++){
        printf("%c",array[i]);
    }
}

void sort(char array[],int len){
    for(int i=0;i<len;i++){
        for(int j=0;j<len;j++){
            if((int)(array[i]-'0')<(int)(array[j]-'0')){
                char temp=array[i];
                array[i]=array[j];
                array[j]=temp;
            }
        }
    }
}

int readarray(char arr[],int len){
    int line_length=0;
    char c;

    while ((c=getchar())!='\n' && line_length<len)
    {
        arr[line_length]=c;
        line_length+=1;
    }

    arr[line_length]='\0';
    
    return line_length;
    
}