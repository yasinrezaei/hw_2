#include<stdio.h>
#include<string.h>

void rotate_array(int arr[],int k);
void printarray(int arr[],int len);

int main()
{ 
    
    int arr[6]={1,2,3,4,5,6};
    int k=3;

    //پس از هر 6 دور به جای خودش برمیگردد
    if(k>6){
        k=k%6;
    }

    rotate_array(arr,k);
    printarray(arr,6);
    
    
    return 0;

}

void rotate_array(int arr[],int k){
    int newarr_1[100];
    int newarr_2[100];
    int i,j;
    for(i=0;i<k;i++){
        newarr_1[i]=arr[i];
    }
    for(i=k;i<6;i++){
        newarr_2[i-k]=arr[i];
    }

    for(j=0;j<6-k;j++){
        arr[j]=newarr_2[j];
    }
    for(j=6-k;j<6;j++){
        arr[j]=newarr_1[j-(6-k)];
    } 
}

void printarray(int arr[],int len){
    for(int i=0;i<len;i++){
        printf("%d",arr[i]);
    }
}

